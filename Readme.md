![coamdo tree](0.jpg "Archivo de Imagen")

## Workshop 02 - Laravel From The Scratch (1° Entregable)

#

En este proyecto estaremos realizadon un curso con Laravel From The Scratch en la cual nos aplira aun mas nuestros conocimiento.

![coamdo tree](1.jpg "Archivo de Imagen")

# Laravel From The Scratch - 2. RoutingLección

# 1) Scratch At a Glance

El este capitulo estuvimos llevando una breve introducción a Laravel, exactamente que sucede cuando llega una solicitud.

![coamdo tree](2.jpg "Archivo de Imagen")

# 5) Basic Routing and Views

Para realizar este episodio neceditaremos levantar todo el proyecto que realizar en el primer workshop para lo cual realizaremos uso de los siguientes comando
`vagrant up`
![coamdo tree](3.jpg "Archivo de Imagen")

Proseguimos con el comando `vagrant rsync-auto` en la ruta establecida
![coamdo tree](4.jpg "Archivo de Imagen")

Por ultimo abrimos el archivo con Visual Code para lo cual usamos `code .`, tomar en cuentar la ubicación
![coamdo tree](5.jpg "Archivo de Imagen")

Ahora si estamos listos para empezar

![coamdo tree](6.jpg "Archivo de Imagen")

Inicialmente nos explica como funcionan las routers y las views, en la cual nos muestrar que el archivo `web.php` ubicado en `router` posee la direccion y el archivo view que se mostara. En este caso seria el `welcome`, el cual se encuntra en `views` como el nombre de `welcome.blade.php`.

Ahora bien sabiendo esto enseña que si creamos otro codigo casi igual pero le cambiomos la ruta entremos el mismo resultado y no se caera el programa. En este caso utiliza la ruta de `/welcome` y carga perfectame la paguina en caso contrario cargaria.

![coamdo tree](7.jpg "Archivo de Imagen")

O bien si cambios la ruta por un texto esta la mostara en pantalla en la direccion que se indica.

![coamdo tree](8.jpg "Archivo de Imagen")
![coamdo tree](9.jpg "Archivo de Imagen")

Luego eliminaremos la rota y creamos un nueva con el nombre de `test`
![coamdo tree](10.jpg "Archivo de Imagen")

Esta misma si se no dara un erro por que no estara creado aun el archivo de test en `views`
![coamdo tree](11.jpg "Archivo de Imagen")

Una vez creado el archivo ingresaremos este codigo y listo.
![coamdo tree](12.jpg "Archivo de Imagen")

Solo faltaria recargar la pagina y terminarias con este episodio.
![coamdo tree](13.jpg "Archivo de Imagen")

# 6) Pass Request Data to Views

Para la realizon de este episodios volveremos a router y web. Donde cambiaremos el codigo de la siguiente manera.
![coamdo tree](14.jpg "Archivo de Imagen")

Y ahora en la ruta ingresaremos en la estas lineas un `?name=David`, posteriomente le daremos recargar y nos mostrara el nombre `David` que le pasomos via url.
![coamdo tree](15.jpg "Archivo de Imagen")

Ahora bien volvemos a cambiar el archivo de router con el siguiente codigo.
![coamdo tree](16.jpg "Archivo de Imagen")

Asi mismo en el archivo test de views se le hacen los siguientes cambios.
![coamdo tree](17.jpg "Archivo de Imagen")

Ya con esto podremos utilizar el parametro pasado por url en un vista html.

![coamdo tree](18.jpg "Archivo de Imagen")
Provado esto procedemos a realizar varios otro ejemplo colocando esta linea en la url `<script>alert('hello');</script>`. Una ves que recargue la paguina nos mostara el `alert` con el mensaje.

![coamdo tree](19.jpg "Archivo de Imagen")

Tambien podemos utilizar esta linea para evitar que se muestre el alert con mensaje que aparece en pantalla solo como texto.
![coamdo tree](20.jpg "Archivo de Imagen")
![coamdo tree](21.jpg "Archivo de Imagen")

Aunque tambien podemos realizar el siguiente cambios y resulta mas simple.
![coamdo tree](22.jpg "Archivo de Imagen")

Luego pare mejorar el codigo reducimos las lineas y listo.
![coamdo tree](23.jpg "Archivo de Imagen")

A bueno bueno una forma simple tambien de postar el alert es de la siguiente forma.
![coamdo tree](24.jpg "Archivo de Imagen")

Como podemos ver tendremos el mismo resultado.
![coamdo tree](25.jpg "Archivo de Imagen")

# 7) Route Wildcards

En este apartado vamos a ligar una misma vista a la ruta `posts/` lo que sea. Ya con ello nos mostara la vista de post las veces que se nos presente un `/` algo.
Para esto cambioamos el codigo de la siguente forma colocando el `/posts/{post}`.
![coamdo tree](26.jpg "Archivo de Imagen")

Al colocar una url a una vista que no exite como es la de post, tendremos que crear una vista con estos datos.
![coamdo tree](27.jpg "Archivo de Imagen")

Ya con ello colocamos una url como la que se muestra y cargara la misma pantalla aunque cambiemos `/a` por `/cola`
![coamdo tree](28.jpg "Archivo de Imagen")

O bien como realizamos antes podemos de igual forma mostar lo que nos pasa por la url, como se logra ver.
![coamdo tree](29.jpg "Archivo de Imagen")

En este caso se ingreso en la url `/bola` y aparece bola.
![coamdo tree](30.jpg "Archivo de Imagen")

Otra forma de utilizar el `post` es creado diferentes mensajes para cada dirección url, en caso creamos solo dos. Para esto tendremos que implementar el siguiente codigo.
![coamdo tree](31.jpg "Archivo de Imagen")

Cambiando la url por la primera dirección, nos mostrando el primer mensaje.
![coamdo tree](32.jpg "Archivo de Imagen")

Mostrado el segundo mensaje, como se lograr ser solo se cambia la url y se cambiara el mensaje automaticamente.
![coamdo tree](37.jpg "Archivo de Imagen")

Aunque bueno tambien podremos realizado unos pequeños cambiosen el codigo, en caso que no sea ninguna de la direcciones establecidas nos muestre otro mensaje algo muy util.
![coamdo tree](33.jpg "Archivo de Imagen")

Al cambiar a uno dirección url no existente me muestra el mensaje.
![coamdo tree](34.jpg "Archivo de Imagen")

Ahora bueno tambien podremos crear un mensaje de `404`.
![coamdo tree](35.jpg "Archivo de Imagen")

Codigo ingresado

```php

    if (!array_key_exists($post, $posts)) {
        abort(404, 'Sorry that pos was not found.');
    }

```

Realizado este cambios a la hora de intentar ingresar a una dirección que no existe, podremos ver el mensaje de `404` en pantalla.
![coamdo tree](36.jpg "Archivo de Imagen")

Ya con esto tendriamos la finalización del este capitulo.

# 8) Routing to Controllers

Para este ejercio creamos una ruta a Controller para ello cambiamos el archivo `web` por los siguiente

```php
    Route::get('/posts/{post}', 'App\Http\Controllers\PostsController@show');

```

Queda de la siguiente manera
![coamdo tree](38.jpg "Archivo de Imagen")

Si cargamos el navegador nos mostara el siguiente error incorrecto
![coamdo tree](39.jpg "Archivo de Imagen")

Para cambiamos nuevamente el archivo `web` por lo siguiente
![coamdo tree](40.jpg "Archivo de Imagen")

Posteriomente crearemos un archivo llamado `PostsControlller` en la ruta 'app/Http/Controller/' en el cual ingresaremos lo siguiente
![coamdo tree](41.jpg "Archivo de Imagen")

Ahora bien si recargamos el navegador nos muestra el mensaje `hello`
![coamdo tree](42.jpg "Archivo de Imagen")

Ahora cambiamos el codigo por el que teniamos anteriormente en `web` el cual seria

```php
    $posts = [
        'my-first-post' => 'Hello, this is my first post!',
        'my-second-post' => 'Now I am getting the hanf of this blogging thing'
    ];

    if (!array_key_exists($post, $posts)) {
        abort(404, 'Sorry that pos was not found.');
    }


    return view('post', [
        'post' => $posts[$post]
    ]);

```

Imagen de como quedaria
![coamdo tree](43.jpg "Archivo de Imagen")

Una vez recargamos el navegador no cargara la informacion inicial y con esto finalizariamos este episodio
![coamdo tree](44.jpg "Archivo de Imagen")

Ya con esto finalizamos los ejercicios de rutas

# Laravel From The Scratch - 3. Database AccessLección

En esta sección trabajaremos con las Database para compreder su funcionar en Laravel

# 9) Setup a Database Connection
En este episodio configuraremos la Database

Aqui podremo apreciar la configuración de la base de datos y en la cual cambiamos el nombre de la base de datos
![coamdo tree](47.jpg "Archivo de Imagen")

La conexion de esta misma 
![coamdo tree](46.jpg "Archivo de Imagen")

Para trabajar con `MySql` utilizaremos `tableplus` el cual descargaremos en `https://tableplus.com/windows`
![coamdo tree](48.jpg "Archivo de Imagen")

Aceptamos terminos y condiciones y le damos todo siguiente
![coamdo tree](49.jpg "Archivo de Imagen")

Una vez instalado lo abrimos y le damos a `Create a new connection`
![coamdo tree](55.jpg "Archivo de Imagen")

Le colocamos lo datos necesarios
![coamdo tree](56.jpg "Archivo de Imagen")

En caso de no tener esa base datos podemos ir a `xamp` y darle administrador en `MySql` donde crearemos una con ese nombre
![coamdo tree](57.jpg "Archivo de Imagen")
![coamdo tree](58.jpg "Archivo de Imagen")

Le damos a test si todo esta bien y si es asi guardamos
![coamdo tree](60.jpg "Archivo de Imagen")

Luego nos aparecera en la pantalla principal del programa y le daremos doble click
![coamdo tree](61.jpg "Archivo de Imagen")

Crearemos una db con el nombre de `posts` y creamos unas columnas y sucesivamente un dato
![coamdo tree](62.jpg "Archivo de Imagen")
![coamdo tree](63.jpg "Archivo de Imagen")


Una vez realizado esto creamos este codifo en `PostsController`
![coamdo tree](64.jpg "Archivo de Imagen")

Nos mostrara el dato
![coamdo tree](65.jpg "Archivo de Imagen")

Aunque podremos realizar unas pequeñas modificaciones para cargar el mensaje el html las cuales serian las siguientes
![coamdo tree](66.jpg "Archivo de Imagen")
![coamdo tree](67.jpg "Archivo de Imagen")

Este seria el resultado
![coamdo tree](68.jpg "Archivo de Imagen")

Con esto se concluye este capitulo

# 10) Hello Eloquent

Buena este episodio sera un tanto rapido estructuraremos un poco el codigo

Inicialmente nos muestran que si cambiamos la dirección url, nos muestra un error en ves del `404`
![coamdo tree](69.jpg "Archivo de Imagen")

Para esto cambiaremo esta pequeñas lineas y listo.
![coamdo tree](70.jpg "Archivo de Imagen")

Ya nos envia el error de un url desconocida
![coamdo tree](71.jpg "Archivo de Imagen")

Ahora se simplificara un poco mas el codigo empesando por el `DB`
![coamdo tree](72.jpg "Archivo de Imagen")

Seguidamente se creara un modulo por lineas de comando en la `PowerShell` con el comado

```

php artisan make:model Post

```

Una vez ejecutado el comando nos mostrara un mensaje
![coamdo tree](73.jpg "Archivo de Imagen")


En el cual crearia este archivo
![coamdo tree](74.jpg "Archivo de Imagen")

Gracias a esto se podrera simplificar mas el codigo
![coamdo tree](75.jpg "Archivo de Imagen")

Esto sin afectar su funcionar
![coamdo tree](76.jpg "Archivo de Imagen")

Por ultimo simplificandolo lo maximo posible quedaria asi,se ve muy elegante el codigo
![coamdo tree](77.jpg "Archivo de Imagen")

Fin de este episodio

# 11) Migrations 101
Este episocio se enfoga en la migración

Para ello nos iremos a `TablePlus` y eliminaremos la tabla creada anteriomente
![coamdo tree](78.jpg "Archivo de Imagen")

Posteriomente se creara una tabla nueva con el comando `php artisan make:migration create_posts_table`
![coamdo tree](79.jpg "Archivo de Imagen")

Como podremos notar en los documento de la web se creo el archivo
![coamdo tree](80.jpg "Archivo de Imagen")

Ahora le realizaremos unos cambios
![coamdo tree](81.jpg "Archivo de Imagen")

Su codigo seria el siguiente:

```php

        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('slug');
            $table->text('body');
            $table->timestamps();
            $table->timestamp('published_at')->nullable();
        });

```

Luego se le realizara una migracion de datos
![coamdo tree](82.jpg "Archivo de Imagen")

Le daremos refrecar en `TablePlus` y apareceran todas las tablas (comando `php artisan migrate`)
![coamdo tree](83.jpg "Archivo de Imagen")

Ahora bien crearemos otra nueva tabla (comando 
`php artisan make:migration add_title_to_posts_table`) y le ingresaremos los datos de title en ambas funciones

![coamdo tree](84.jpg "Archivo de Imagen")

Nuevamente se migran los datos para ver los cambios
![coamdo tree](85.jpg "Archivo de Imagen")

En este ocasion se crearia el campo `title`
![coamdo tree](86.jpg "Archivo de Imagen")
Comprobado esto aplicamos el `php artisan migrate:rollback` para deshacer los cambioss
![coamdo tree](87.jpg "Archivo de Imagen")

Luego eliminaremos manualmente la tabla `add_title_to_posts_table` y en  create post colocamos ahora el dato `title`
![coamdo tree](89.jpg "Archivo de Imagen")

Comprovado esto nuevamente se le hace un `rollback` y se migran los datos
![coamdo tree](90.jpg "Archivo de Imagen")

Por ultimo se le dara `php artisan migrate:fresh`
![coamdo tree](91.jpg "Archivo de Imagen")

Con esto finaliza este curso

# Laravel From The Scratch - 4. Views

# 14) Layout Pages
En este episodio trabajaremos sobre los layout

Para primero creamos un archivo nuevo con el nombre de `layout.blade.php` que estara en view y luego de cortara el codigo de `welcome.blade.php` y se lo pegara en el antes creado
![coamdo tree](92.jpg "Archivo de Imagen")

Posteriomente se costara a `Layout` todo el contenido la etiqueta `body` y se pegara nuevamente al archivo welcome
![coamdo tree](93.jpg "Archivo de Imagen")

Al intentar recargar el documento quedara de la siguiente manera
![coamdo tree](94.jpg "Archivo de Imagen")

Para solucionarlos creamos un `extends de layout` en el documento `welcome`
![coamdo tree](95.jpg "Archivo de Imagen")

Seguidamente en el archivo `layout` colocaremos en el `body` lo siguiente
![coamdo tree](96.jpg "Archivo de Imagen")

A su vez en el `welcome` cortaremos todo lo que se encuentre debajo del `extends`, y creamos la estructura de la section y colocaremos el codigo cortado en medio de los section
![coamdo tree](97.jpg "Archivo de Imagen")

Ya con esto recargamos y todo solucionado
![coamdo tree](98.jpg "Archivo de Imagen")

Otra pueda es esto es la creacion de un archivo `contact`
![coamdo tree](99.jpg "Archivo de Imagen")

Colocamos el siguiente codigo en el archivo
![coamdo tree](100.jpg "Archivo de Imagen")

Por ultimo, obtendremos lo siguiente:
![coamdo tree](101.jpg "Archivo de Imagen")

Fin de este capitulo


# 15) Integrate a Site Template

En este episodio se intengran los template

Para ello descargaremos uno en la ruta `https://templated.co/simplework`, y una vez descargado moverenos los archivos todos los archivos excepto el index a la carpeta publica de laravel.

![coamdo tree](102.jpg "Archivo de Imagen")

Poderiormete crearemos dos archivos para crear la esctructura
![coamdo tree](103.jpg "Archivo de Imagen")

En el caso del archivo `welcome` solo crearemos el extend
![coamdo tree](104.jpg "Archivo de Imagen")

En el caso del `layoutv2` copiaremos la informacion del archivo `ìndex` que descargamos
![coamdo tree](105.jpg "Archivo de Imagen")

Una vez ello estos cambios ya deria de cargar, obvio verifique las ruta esten bien
![coamdo tree](106.jpg "Archivo de Imagen")

Luego ordenamos un poco el codigo creado un carpeta css y cambiando la ubicacion de estos archivos
![coamdo tree](107.jpg "Archivo de Imagen")

Le `layout` creamos en el body un  enlace al contenido
![coamdo tree](108.jpg "Archivo de Imagen")

Este contenido se encuentra en `welcome`
![coamdo tree](109.jpg "Archivo de Imagen")

Luego copiarems este `wrapper`
![coamdo tree](110.jpg "Archivo de Imagen")

Crearemos toda la estructura de una nueva ruta y archivos con `about`
![coamdo tree](112.jpg "Archivo de Imagen")

Pegamos una copia del archivo container de layout
![coamdo tree](113.jpg "Archivo de Imagen")

Ligamos un archivo con otro 
![coamdo tree](114.jpg "Archivo de Imagen")

Ya con estos estariamos listos, fin del episodio

# 16)Set an Active Menu Link

Este es un episodio muy rapido para la creacion de un menu link.

Para esto nos ubicaremos en `layout` y bucamos el div `menu` donde estan los link
![coamdo tree](115.jpg "Archivo de Imagen")

Comenzaremos a cambiarlos uno por uno por la informacion requerida
![coamdo tree](116.jpg "Archivo de Imagen")

Una vez cambiados quedarian de la siguiente forma, sin lugar a dudo es muy rapido la configuracion de los link
![coamdo tree](117.jpg "Archivo de Imagen")

# 17) Asset Compilation with Laravel Mix and Webpack

En es apartado realizaremos la restructuracion de mucho codigo, iniciado con cambir el `copyrignt` elimando de todas las pagina y dejadolo en layout
![coamdo tree](118.jpg "Archivo de Imagen")



Laravel proporciona una herramienta útil llamada Mix, un envoltorio de paquete web, para ayudar con la agrupación y compilación de activos. 
![coamdo tree](119.jpg "Archivo de Imagen")
En el siguiente apartado colocaremos los estilos de nuestro proyecto para la hoja de estilo donde declaramos una variable con el $primary el cual tendra el color rojo = red, se utiliza sass por que es muy util y facil, por que podemos declarar variables que estaremos necesitando en varias partes en la hoja de estilo y de esta manera es mas rapido y facil
![coamdo tree](120.jpg "Archivo de Imagen")

A continuacion abriremos la consola donde estamos trabajando e instalaremos algunos paquetes con el siguiente comando  `npm install`
![coamdo tree](121.jpg "Archivo de Imagen")
Luego con el siguiente comando `nmp run dev`  corremos el proyecto
![coamdo tree](122.jpg "Archivo de Imagen")
Nos muestra un error que nos dice que se esperaba `;` 
![coamdo tree](123.jpg "Archivo de Imagen")
asi volvemos al archivo que de sassas para arreglar el problema y ponerle en `;`
![coamdo tree](124.jpg "Archivo de Imagen")
Luego volvemos a ejecturar el comando el cual no deberia de tener mas problemas
![coamdo tree](125.jpg "Archivo de Imagen")
Corremos el siguiente comando `npm run whatch`
![coamdo tree](126.jpg "Archivo de Imagen")

![coamdo tree](127.jpg "Archivo de Imagen")
![coamdo tree](128.jpg "Archivo de Imagen")




# 18) Render Dynamic Data

Se aprendera a representar datos dinámicos. La página "acerca de" de la plantilla de sitio que estamos usando contiene una lista de artículos. Creemos un modelo para estos, almacenemos algunos registros en la base de datos y luego los rendericemos dinámicamente en la página.
Vamos a crear un nuevo modelo que cual se llamara `Article` 
![coamdo tree](129.jpg "Archivo de Imagen")
Luego vamos a crear una tabla llamda `Article` en migracion 
![coamdo tree](130.jpg "Archivo de Imagen")
Asi deberia de verse 
![coamdo tree](131.jpg "Archivo de Imagen")
 y ahora en la parte de schema crearemos unos campos para la tabla de `Article`
![coamdo tree](132.jpg "Archivo de Imagen")
Luego migraremos la tabla articles
![coamdo tree](134.jpg "Archivo de Imagen")
ahora agregaremos datos a esa tabla 
![coamdo tree](135.jpg "Archivo de Imagen")
Podemos observar que la tabla tiene datos
![coamdo tree](136.jpg "Archivo de Imagen")
Y por ultimo guardamos los datos 
![coamdo tree](137.jpg "Archivo de Imagen")
Ahora en el archivo de routes modificamos para que  nos muestre todos los articulos que se encuentren en la base de datos con la porpiedad `all`
![coamdo tree](138.jpg "Archivo de Imagen")
Con la propiedad `take(2)` solo mostrara dos articulos 
![coamdo tree](139.jpg "Archivo de Imagen")
Y con  `lates('')` se puede pedir espeficicamente que muestre algun dato 

![coamdo tree](143.jpg "Archivo de Imagen")
Ahora para mostrar esos datos en la vista se tiene que recorrer y para eso lo haremos con un `@foreach` el cual tendra los datos de la tabla articulo
![coamdo tree](144.jpg "Archivo de Imagen")
 Y ahora con `$article->except` mostrara los datos de ese campo en especifico
![coamdo tree](145.jpg "Archivo de Imagen")
Y asi se mostraria los datos 
![coamdo tree](146.jpg "Archivo de Imagen")


# 19) Render Dynamic Data: Part 2
Se creara una página dedicada para ver un artículo completo y especifico.
Primero agregaremos en el menu una nueva etiqueta para `Article`
![coamdo tree](147.jpg "Archivo de Imagen")
Volvemos al navegador y resfrescamos la pagina y se notara ahora en el menu 
![coamdo tree](148.jpg "Archivo de Imagen")
En la routes agregaremos una nueva ruta para Articles
![coamdo tree](149.jpg "Archivo de Imagen")
Luego crearems un `Controller` para el articulo con el nombre de `$ArticlesController`
![coamdo tree](150.jpg "Archivo de Imagen")
En el controler en la fuction de mostrar pondremos que debemos de recibir como parametro `$id` del articulo que se quiere ver y el cual retornara ese articulo
![coamdo tree](151.jpg "Archivo de Imagen")
crearemos una nueva carpeta llamada `articles` y en el cual crearemos un archivo el cual mostrara el articulo escogido `show.blade.php` 
![coamdo tree](152.jpg "Archivo de Imagen")
Copiaremos lo que teniamos en la otra pagina en esta 
![coamdo tree](153.jpg "Archivo de Imagen")
Y se mostrara de esta manera sin el diseño solo texto
![coamdo tree](154.jpg "Archivo de Imagen")
para arreglar el error anterio a las etiques donde estan las rutas de los estilos se le agregara `/` ya que esta pagina se encuentra dentro de una carpeta nueva y por eso no muestra los estilos
![coamdo tree](155.jpg "Archivo de Imagen")
como se nota en la imagen no se muestra la imagen 
![coamdo tree](156.jpg "Archivo de Imagen")
ese error se debe que es igual que el anterior con los estilos asi que solo se debe de  agregar `/`a la etiqueta de imagen
![coamdo tree](157.jpg "Archivo de Imagen")
y solucionaremos el error 
![coamdo tree](158.jpg "Archivo de Imagen")
Ahora eliminaremos el texto que teniamos y pocemos a colocar ya los datos que se necesitan para mostrar la informacion del articulo escogido que seria de la siguiente manera 
![coamdo tree](159.jpg "Archivo de Imagen")
Y como se puede ver se puede ver el articulo del `$id = 1`
![coamdo tree](160.jpg "Archivo de Imagen")
Ahora en otro archivo de la vista donde se muestra una seccion de los articulos modificaremos el codigo para que cada vez que seleccione alguno lo lleva a la otra pagina a ver ese articulo
![coamdo tree](161.jpg "Archivo de Imagen")
![coamdo tree](162.jpg "Archivo de Imagen")
![coamdo tree](163.jpg "Archivo de Imagen")
Ahora al menu agregamos que queremos que nos lleve a la ruta de `Articulos`
![coamdo tree](164.jpg "Archivo de Imagen")
Pero a la hora de presionarlo nos dara error ya que esta no se encuentra aun, solo los articulos especificos 
![coamdo tree](165.jpg "Archivo de Imagen")
![coamdo tree](166.jpg "Archivo de Imagen")

# 20) Homework Solutions
Para mostrar una lista de artículos, deberá crear una ruta coincidente, una acción de controlador correspondiente y la vista para iterar sobre los artículos y representarlos en la página.

Para arreglar el error anterior debemos hacer lo siguiente primero el las routas colocaremos un nueva ruta `ArticlesController@index`
![coamdo tree](167.jpg "Archivo de Imagen")
Ahora en el controller crearemos una nueva funcion la cual se llamara `index ` el cual obtendra todos los datos de los articulos y los retornara a la vista de index
![coamdo tree](168.jpg "Archivo de Imagen")
Creamos una nueva vista en el view el cual llamaremos `index ` en el cual recorreremos los datos mediante un `foreach ` y ademas de eso le agregamos en una etiqueta le colocamos la ruta para qye cada vez toque un articulo lo lleve a ver ese articulo en especifico 
![coamdo tree](170.jpg "Archivo de Imagen")
![coamdo tree](171.jpg "Archivo de Imagen")

# Laravel From The Scratch - 5. Forms
# 21) The Seven Restful Controller Actions
tenemos varios diferentes de action como el `create,store, edit,update, view,  destroy`

![coamdo tree](172.jpg "Archivo de Imagen")
Ahora crearemos un nuevo controller el cual llamaremos `ProjectController` el cual contendra los procedmimiento de 
 `create,store, edit,update, view,  destroy`
![coamdo tree](173.jpg "Archivo de Imagen")


# 21) The Seven Restful Controller Actions
Hay siete acciones  del controlador con las que se deberian de  familiarizarse. 
En este caso estas acciones las colocaremos tambien ene el controller del Articulo 
![coamdo tree](174.jpg "Archivo de Imagen")


# 22) Restful Routing
Ahora  volvemos  a la capa de enrutamiento y revisemos un enfoque RESTful para construir URI y comunicar la intención.

En este apartado detremo una breve explicacion de cada metodo en el html, entre ello estan
![coamdo tree](175.jpg "Archivo de Imagen")

# 23) Form Handling

Se crea el formulario para insertar los datos del articulo
![coamdo tree](176.jpg "Archivo de Imagen")

En este apartado creamos el enrutamiendo de cada controller, un manera super vital hoy en dia
![coamdo tree](177.jpg "Archivo de Imagen")

Posteriomente mostraremos los articulo
![coamdo tree](178.jpg "Archivo de Imagen")

Para ello creariamos esta funcion con cada dato estructura de los articulo
![coamdo tree](179.jpg "Archivo de Imagen")

Con ello apredemos la forma de realizar un insert

# 24) Forms That Submit PUT Requests

Dado que los navegadores solo aceptan get y post. El put se realiza diferente 

Para esto realizamos el enrutamiento para transportar los datos 
![coamdo tree](180.jpg "Archivo de Imagen")

Luego se creara las funciones para editar 
![coamdo tree](181.jpg "Archivo de Imagen")

Posteriomente crearemos un nuevo archivo y modificaremos los datos, es muy similar al editar
![coamdo tree](182.jpg "Archivo de Imagen")

En este caso se busca el articulo por medio del id
![coamdo tree](183.jpg "Archivo de Imagen")

Creamos el enrutamiento para actualizar los datos
![coamdo tree](184.jpg "Archivo de Imagen")

Este seria la funcion para actualizar los datos en el formulario
![coamdo tree](185.jpg "Archivo de Imagen")

Algunos navegadores no aceptan lo put por lo cual se le agregara lo siguiente
![coamdo tree](186.jpg "Archivo de Imagen")

Ya con esto estariamos listo en este episodio

# 25) Form Validation Essentials

En este apartado trabajaremos para validar los errores de diferentes formas 


Inicialmente de esta forma
![coamdo tree](187.jpg "Archivo de Imagen")

Posteriomente se realizara la validacion en en el input  por una propiedad
![coamdo tree](188.jpg "Archivo de Imagen")

Crearemos una validacion, para los errores
![coamdo tree](189.jpg "Archivo de Imagen")

Con esto terminariamos el capitulo