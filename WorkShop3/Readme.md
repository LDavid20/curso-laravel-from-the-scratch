# Workshop 03 - Laravel From The Scratch (2° Entregable)

## Laravel From The Scratch - 6. Controllers Techniques

### 26) Leverage Route Model Binding
En este capitulo nos enseña como pasar de estructura manual para mostarar articulos a unos mas automatico.

Para lo cual cambiaremos el metodo de mostrar de la siguiente forma, este nos cargara el articulo mostara con el id `1`.
![coamdo tree](1.jpg "Archivo de Imagen")


Posteriomente nos cambiaremos el nombre de la variable y por ente entremos que cambiar el nombre de la variable del metodo show
![coamdo tree](2.jpg "Archivo de Imagen")

Cabe denotar que podremos realizar una simplificación de metodo de la siguiente forma, y luego cambiarle el nombre a la variable.
Gracias a estos pequeños cambios lograremos mostra los articulos de una manera mas automatica   
![coamdo tree](3.jpg "Archivo de Imagen")

## 27) Reduce Duplication

Para este siguiente episodio se reducira duplicidad.

Para ello nos dirigimos al archivo de `ArticlesController` y modificaremos el metodo de `store` de la siguiente forma, esto nos obligara a la hora de crear los articulos requerirlos.

![coamdo tree](4.jpg "Archivo de Imagen")


Pero buenos es posible mejorar esto de la siguiente forma, dirigiendonos al archivo `Article` y creado dentro de el la siguiente linea que busca proteger los atributos del articulo.
![coamdo tree](6.jpg "Archivo de Imagen")

Ahora bien para simplificar esto y evitar que por un lado los valide y por otro los requira, se realizara la siguiente combinacion que busca reducir el codigo siendo lo bastante optimo posible. Para esto nuevamente nos vamos al metodo de `store` en el archivo de `ArticleController` y lo cambiamos de la siguiente forma.

![coamdo tree](7.jpg "Archivo de Imagen")

Cabe denotar que esto mismo se le realizara al metodo de `update`, para asi aplicar los mismo en el metodo.
![coamdo tree](8.jpg "Archivo de Imagen")
## 28) Consider Named Routes

En este capitulo se busca realizara un proceso en el cual nos permita actualizar de manera automatica todos los enlases si se llegara a cambiar un ruta


Para esto colocaremos el nombre a una ruta de la siguiente forma.
![coamdo tree](9.jpg "Archivo de Imagen")

Ahora a la hora de crear el link a la ubicacion de articulos lo llamos por el nombre del siguiente forma
![coamdo tree](10.jpg "Archivo de Imagen")

Esto se le aplicaria a otrar rutas 
![coamdo tree](11.jpg "Archivo de Imagen")

Asi mismo como a las funciones se le podria aplicar este metodo
![coamdo tree](12.jpg "Archivo de Imagen")

Aunque bueno tambien en el archivo `Article` podremos crear una metodo para que retorne esta ruta y volverla aun mas simple
![coamdo tree](13.jpg "Archivo de Imagen")

Como se lograr apreciar es una forma muy eficar de aplicar este metodo
![coamdo tree](14.jpg "Archivo de Imagen")

## Laravel From The Scratch - 7 Eloquent 29)Basic Eloquent Relationships

En este episodio se busca crear relaciones entre el usuarios, los articulos y los proyectos. Para ello se realizara el siguiente procedimiento 

En el archivo `Project` se reara la siguiente linea, en el cual busca selecionar todos los usarios dondel el proyecto sea `x` ejemplo 1
![coamdo tree](15.jpg "Archivo de Imagen")

Tomando esto en cuenta nos vamos al archivo `User`, crearemos la siguiente linea en las cuales nos permetira ahora cargar los articulos por user asi mismo cargar los proyectos por usuario.    

Y ya realizado estos cambios terminaremos este capitulo

![coamdo tree](16.jpg "Archivo de Imagen")

## 30) Understanding Foreign Keys and Database Factories

Para iniciar este capitulo primero crearemos nueva columna en la tabla articulos, en este caso `user_id`

Para esto eliminaremos la table le asignamos el nuevo valor y los volvemos a cargar, estos camios se realizan en el archivo de `create articule table`
![coamdo tree](17.jpg "Archivo de Imagen")

Ahora bien gracias al archivo de `UserFactory` recargaremos los cambios de la tabla de una manera rapida
![coamdo tree](18.jpg "Archivo de Imagen")


Una ves compredido como funciona, creamos un archivo similar para recargar los articulos con el siguiente comando `php artisan make:factory ArticleFactory` que nos creara el archivo y le colocamos los atributos necesarios

![coamdo tree](19.jpg "Archivo de Imagen")

Ahora bien compredido este puto vamos a crear el siguiente codigo que nos permitira eliminar el usuario y quitar todo los que los relacione gracias a que es un metodo en cascada 
![coamdo tree](20.jpg "Archivo de Imagen")

Compredido esto nos dirigimos ahora al archivo de `User.php` donde estableceremos la siguiente linea y ya con esto crearimos todo lo necesarios para la relaciones
![coamdo tree](21.jpg "Archivo de Imagen")

## 31) Many to Many Relationships With Linking Tables

Para este nuevo capitulo se buscara crear relaciones de muchos entre muchos, para lo cual realizaremos el siguiente proceso,
Para iniciara nos dirigiremos al archivo de `Article` y le crearemos los siguentes campos.

![coamdo tree](22.jpg "Archivo de Imagen")

A su ves nos dirigiremos al archivo de `create tags table` y  ingresaremos los siguiente datos, esto dato busca crear dos tablas una para los `tags` y otra para la relacion entre los articulos y los `tags`

![coamdo tree](23.jpg "Archivo de Imagen")

Para finalizar nos dirigiremos al archivos de `Tag.php` y creamos los siguiente, esto nos permetire ver el articulo relacionado con el tag
![coamdo tree](24.jpg "Archivo de Imagen")

Con esto se concluye este capitulo

## 32) Display All Tags Under Each Article

Buenas en este capitulo se buscara mostrar todas las etiquetas de los articulos para lo cual realizaremos lo siguiente 

Nos dirigimos al archivo de  `show.blade.php` y creamos las siguientes lineas despueste del body que nos servira para mostrar los tag, cabe mencionar que en esta ocación le colocamos un stylo para que se viera mejor 
![coamdo tree](25.jpg "Archivo de Imagen")

Ahora bien una vez realizado lo anterior, nos vamos ahora al archivo `ArticlesController.php` en la  funcion index, estos nos servira para cargar los tag y controlar los posibles errores.
![coamdo tree](26.jpg "Archivo de Imagen")

Tomando esto en cuenta ahora vamos `index.blade.php` y modificamos el `foreach` para adactarlo a nuestra nuevas necesidades, asi mismo para controlar errores 
![coamdo tree](27.jpg "Archivo de Imagen")

Y para no olvidarnos de la nuevas rutas, realizamos el cambio por el nuevo formato de rutas
![coamdo tree](28.jpg "Archivo de Imagen")

Un episodio bastante sencillo pero a la vez necesarios para un buena creacion de sistema

## 33)Attach and Validate Many-to-Many Inserts

En este episodio se realizara inserciones apochandonos en metodo de attach() y detach() que insertar uno o varios registros a la vez.

Para ello nos vamos inicialmente y modificamos el formulario de crear en el archivo de `create.blade.php` de la siguiente manera.
![coamdo tree](29.jpg "Archivo de Imagen")

Una vez realizado esto, ahora continuamos con el metodo  crear colocandolo de la siguiente forma, ya con esto nos mostrar los todos los tag el el `select`
![coamdo tree](30.jpg "Archivo de Imagen")
Exelente, comprendido lo anterios vamos a `ArticleController` y cambiamos la funcion de store, que nos permetira guardar los articulos del usuario 1 creado asi una relacion
![coamdo tree](31.jpg "Archivo de Imagen")

Ahora bien, tocara cambira la funcion de `validateArticule` para evitar que vayan vacios los datos del tag
![coamdo tree](32.jpg "Archivo de Imagen")

Y bueno ahora si con esto volvemos a `create.blade.php` y habilitaremos la selección multiple en el select
![coamdo tree](33.jpg "Archivo de Imagen")

Ahora volvemos al `ArticleController` para inicializar los datos cuando creamos el dato
![coamdo tree](34.jpg "Archivo de Imagen")

Por ultimo, en la funcion de tag que esta en `Articule`, realizamos una pequeña edicion para que asigne tambien el timestamps
![coamdo tree](35.jpg "Archivo de Imagen")

Con esto damos finalizados el capitulo de Eloquent


## Laravel From The Scratch - 8 Authentication 34)Build a Registration System in Mere Minutes


Para esto crearemos un nuevo proyecto en larabel y en el mismo PowerShell colocaremos el siguiente comando
![coamdo tree](36.jpg "Archivo de Imagen")

Tomando esto en cuenta seguimos con la siguimos linea instalar `vue` una vez finalizado el primer comando, el damos `npm install $$ npm run dev` ya con esto estaremos listos para trabajar
![coamdo tree](37.jpg "Archivo de Imagen")

Posteriomente creamos un conexion en `TablePlus` con MySql y migramos la tables con el comando de `php artisan migrate`
![coamdo tree](38.jpg "Archivo de Imagen")

Ahora bien si quisieramos realizar una pueba y quitar la autorizacion para ingresar al inicio, solo tenemos que ir al `HomeController` comentar la linea de `middleware('auth')`.
![coamdo tree](39.jpg "Archivo de Imagen")
 Ahora bien realizaremos una nueva forma de autenticar dejando esa linea anterior comentada, para ello nos vamos a `web.php` y  modificamos el route
![coamdo tree](40.jpg "Archivo de Imagen")

Ahora bien si quisieramos mostar el nombre del usuario a la hora de ingresar solo tendriamos que realizar lo siguiente, ejemplo muy rapido y simple
![coamdo tree](41.jpg "Archivo de Imagen")

Ahora bien vamos aplicar esta misma tecnica en el archivo de `welcome` para lo cual reammos estos cambios asi si incresamos con un usuario nos aparecera el nombre de lo contrario solo Laravel
![coamdo tree](42.jpg "Archivo de Imagen")

O bien otra opcion seria realizarlo de la siguiente forma
![coamdo tree](43.jpg "Archivo de Imagen")

## 35) The Password Reset Flow
En este episodio se busca restablece una contraseña en caso de que el usuaio se le haya olvidado para lo cual realizaremos una serie de procesos

Estos procesos serian
![coamdo tree](44.jpg "Archivo de Imagen")

Para esto vamos a tener que modificar en el archivo `.env` el dado de MAIL_DRIVER por `log`
![coamdo tree](45.jpg "Archivo de Imagen")

Ya con   esto tendremos el punto numero 3 y 4 listos

Ahora bien lo ultimo nos digimos al link siguiente con el toquen asignado y cambiamos la contraseña, cabe mencionar que si cambiamos el token no se cambiara la contraseña eliminado asi el toke. Ya con esto complimos con los 5 puntos
![coamdo tree](46.jpg "Archivo de Imagen")

Sin mas que decir, esto seria como cambiar la contraseña.


## Laravel From The Scratch - 9 Core Concepts 36) Collections

Para este episodio nos centraremos en el encademanimiento de colecciones, y asi mostar un conjuto de ejemplo con la gran cantidad de colleciones hay manipulado metodos
Como podemos mostrar esto son varios metodos que podriamos utilizar en una coleccion
![coamdo tree](47.jpg "Archivo de Imagen")

Varias de estas colecciones ejecutadas serian:

1) 
![coamdo tree](48.jpg "Archivo de Imagen")

2)
![coamdo tree](49.jpg "Archivo de Imagen")

3)
![coamdo tree](50.jpg "Archivo de Imagen")

4)
![coamdo tree](51.jpg "Archivo de Imagen")

5)
![coamdo tree](52.jpg "Archivo de Imagen")

6)
![coamdo tree](53.jpg "Archivo de Imagen")

7)
![coamdo tree](54.jpg "Archivo de Imagen")

8)
![coamdo tree](55.jpg "Archivo de Imagen")

9)
![coamdo tree](56.jpg "Archivo de Imagen")

10)
![coamdo tree](57.jpg "Archivo de Imagen")

11)
![coamdo tree](58.jpg "Archivo de Imagen")

12)
![coamdo tree](59.jpg "Archivo de Imagen")


Con esto podriamos ver varios ejemplos de colecciones

## 37) CSRF Attacks, With Examples

En este capitulo de busca crear un ataque de CSRF y como crear una proteccion ante este posible ataque

Pero en que consiste el `CSRF`, es el apoderamiento de una seccion de un usuario con el fin de perjudicarlo o obtener algun tipo de informacion 

Para ello nos vamos al archivo de `web.php` y descomentamos la siguiente linea
![coamdo tree](60.jpg "Archivo de Imagen")

Ahora bien podremos escribir lo siguiente en el archivo `welcome.blade.php`, mostrado como realizar un deslogue desde otra ventana brindado asi el poder a otro usuario
![coamdo tree](61.jpg "Archivo de Imagen")

Compredido este aspecto, eliminamos las lineas que abismos descomentado en `web.php`
![coamdo tree](62.jpg "Archivo de Imagen")

Nuevamente volvemos al `welcome.blade.php` y creamos el siguiente formulario para desloguearnos, ya con esto lograremos evitar que se nos inflitren por medio de CSRF
![coamdo tree](63.jpg "Archivo de Imagen")

## 38) Service Container Fundamentals

En este capitulo veremos los fundamentos sobre los contenedores de servicios 

Para este ejemplo crearemos dos archivos uno llamo `Container` y otro llamado ejemplo.

Ahora bien nos dirigimos al archivo de `web.php`
![coamdo tree](64.jpg "Archivo de Imagen")
Continuamos en el archivo de container y digidamos el sigueinte codigo
![coamdo tree](66.jpg "Archivo de Imagen")

Ahora bien colocamos alguna informacion en el ejemplo
![coamdo tree](67.jpg "Archivo de Imagen")

Realizamos una pequeña modificacion en el archivo de `web.php`
![coamdo tree](68.jpg "Archivo de Imagen")

Con esto finalizamos este episodio

## 39) Automatically Resolve Dependencies

Ahora que comprendemos  los conceptos básicos de un contenedor de servicios, pasemos a la implementación de Laravel. Como verá, además de lo básico, también puede, en algunos casos, construir objetos automáticamente para usted. Esto significa que puede "pedir" lo que necesita, y Laravel hará todo lo posible, utilizando la API de reflexión de PHP, para leer el gráfico de dependencia y construir lo que necesita.


El contenedor de servicios de Laravel es una herramienta poderosa para administrar las dependencias de clases y realizar la inyección de dependencias. La inyección de dependencia es una frase elegante que esencialmente significa esto: las dependencias de clase se "inyectan" en la clase a través del constructor o, en algunos casos, los métodos "setter"

En este caso pediremos de  la clase Example algunos datos
![coamdo tree](69.jpg "Archivo de Imagen")
En el cual no devolvera un numero
![coamdo tree](70.jpg "Archivo de Imagen")

Ahora en la clase Example, haremos lo siguiente
![coamdo tree](71.jpg "Archivo de Imagen")
A lo cual si vamos a la vista y recargamos la pagina nos mostrara lo siguiente
![coamdo tree](72.jpg "Archivo de Imagen")

## 40) Laravel Facades Demystified
Ahora podemos pasar a las fachadas de Laravel, que proporcionan una interfaz estática conveniente para todos los componentes subyacentes del marco. En esta lección, revisaremos la estructura básica, cómo rastrear la clase subyacente y cuándo puede optar por no usarlas.


Se crea un constructor en el Factory 
![coamdo tree](73.jpg "Archivo de Imagen")

 las llamadas desde su fachada a un objeto resuelto desde el contenedor. En el siguiente ejemplo, se realiza una llamada al sistema de caché de Laravel. getCache
![coamdo tree](74.jpg "Archivo de Imagen")

## 41) Service Providers are the Missing Piece

Un proveedor de servicios es una ubicación para registrar enlaces en el contenedor y configurar su aplicación en general.

Los proveedores de servicios son el lugar central de todas las aplicaciones de arranque de Laravel. Su propia aplicación, así como todos los servicios principales de Laravel, se ejecutan a través de proveedores de servicios.

![coamdo tree](75.jpg "Archivo de Imagen")
Como se mencionó anteriormente, dentro del registermétodo, solo debe vincular cosas en el contenedor de servicios . Nunca debe intentar registrar ningún  eventos, rutas o cualquier otra función dentro del registermétodo. De lo contrario, puede utilizar accidentalmente un servicio proporcionado por un proveedor de servicios que aún no se ha cargado.
![coamdo tree](76.jpg "Archivo de Imagen")
El metodo  boot se llama después de que se hayan registrado todos los demás proveedores de servicios , lo que significa que tiene acceso a todos los demás servicios que han sido registrados por el marco, pero no lo necesitamos en este caso
![coamdo tree](77.jpg "Archivo de Imagen")

Con esto concluimos todos los episodios del tercer Workshop