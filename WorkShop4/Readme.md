![coamdo tree](0.jpg "Archivo de Imagen")

# Workshop 04 - Laravel From The Scratch (3° Entregable)



## Laravel From The Scratch - 10 Mail 42) Send Raw Mail
En este episodio,  se realizara una serie de procedimiento las cuales busca realizar un envio de mensaje por medio del metodo `Mail::raw()`.

![coamdo tree](1.jpg "Archivo de Imagen")
Primero realizaremos uno pequeño formulario con un input de email
Se realizara con el siguiente codigo:

![coamdo tree](2.jpg "Archivo de Imagen")
![coamdo tree](3.jpg "Archivo de Imagen")
Posteriomente nos dirigiremos a las `web.php` y crearemos una ruta post para el envio del correo

![coamdo tree](4.jpg "Archivo de Imagen")
Ahora bien dentro del archivo de `ContactController.php` vas a colocar este codigo para realizar una prueba a la hora de enviar un mensaje

![coamdo tree](5.jpg "Archivo de Imagen")
Ahora bien ligamos la accion del formulario a la ruta de contact y store

![coamdo tree](6.jpg "Archivo de Imagen")
Realizado esto cuando coloquemos un texto y le daremos al boton 

![coamdo tree](7.jpg "Archivo de Imagen")
Este nos mostrara el correo

![coamdo tree](8.jpg "Archivo de Imagen")
Cabe denotar que este no crea un validacion por si es un correo, por lo cual crearemos el siguiente codigo para controlar ese error 

![coamdo tree](9.jpg "Archivo de Imagen")
Probado quedaria de la siguiente forma

![coamdo tree](10.jpg "Archivo de Imagen")

Ahora bien comprobado este aspecto vamos a modificar el codigo anteriormente creado por el siguiente que nos realizara el envio del mensaje 

![coamdo tree](11.jpg "Archivo de Imagen")
Este mensaje que guardado en `log` que se encuentra es store, asi como esta en la imagen, todo esto puede variar si cambiamos la configuracion en `.env`

![coamdo tree](12.jpg "Archivo de Imagen")
Mensaje que se envio  con varios daros por defectos

![coamdo tree](13.jpg "Archivo de Imagen")
En este caso el email por defecto es `hello@example.vom`

![coamdo tree](14.jpg "Archivo de Imagen")
Para cambiar esto vamos otra ves al archivo `.env` y creamos la siguietne linea para que coloque el email que necesitamos

![coamdo tree](15.jpg "Archivo de Imagen")
Una vez enviado un mensaje quedaria de la siguiente forma

![coamdo tree](16.jpg "Archivo de Imagen")
Para crear un mensaje de enviado luego de enviar el mensaje creamos lo siguiente en el metodo de `store`

![coamdo tree](17.jpg "Archivo de Imagen")
Y para mostarlo abra que tambien que colocar lo siguiente en la parte grafica

![coamdo tree](18.jpg "Archivo de Imagen")

Asi quedaria
![coamdo tree](19.jpg "Archivo de Imagen")

Podemos realzar una modificaciones esticas
![coamdo tree](20.jpg "Archivo de Imagen")

Quedando asi
![coamdo tree](21.jpg "Archivo de Imagen")

Ya con esto finalizariamos este episodio


## 43)  Simulate an Inbox using Mailtrap

En este episodio creamos una simulacion de una banteja de entrada de correos.

Para esto iniciaremos una cuenta en mailtrap, nos dirigimos a `maintrap.io` y creamos nuestra cuenta o nos logueamos con la cuenta de google
![coamdo tree](22.jpg "Archivo de Imagen")
Posteriormente creamos una inbox en este caso se llama demo
![coamdo tree](23.jpg "Archivo de Imagen")
Una ves creado esto le damos click a inbox creado llamado `demo` y nos aparecera su configuracion
![coamdo tree](24.jpg "Archivo de Imagen")
Esta misma configuracion colocaremos en nuestro archivo `.env` de la siguiente forma
![coamdo tree](25.jpg "Archivo de Imagen")
Volvemos a realizar un envio de mensaje y ahora este apareceria en el inbox de demo
![coamdo tree](21.jpg "Archivo de Imagen")
![coamdo tree](26.jpg "Archivo de Imagen")

Con esto terminariamos este capitulo

## 44) Send HTML Emails Using Mailable Classes

Llegado a este punto solo hemos podidos crear un mensaje de correo de manera sentilla, pero en este episodio trataremos de mejorar este mensaje.
<!-- ![coamdo tree](27.jpg "Archivo de Imagen") -->

Primero iniciamos crando un archivo de mensaje con el siguiente comando de artisan, en este caso yo lo realizaria por medio de `PowerShell`
![coamdo tree](28.jpg "Archivo de Imagen")
Una ves creado el archivo entramos en el y realizamos la siguiente modificacion
![coamdo tree](29.jpg "Archivo de Imagen")
Posteriomete nos vamos al archivo de emaik y tambien creamos otro cambios pequeño
![coamdo tree](30.jpg "Archivo de Imagen")
Contuniando no lo mismo volvemos a la funcion de store que en el capitulo pasado estuvimos trabajando y realizamos el enlace
![coamdo tree](31.jpg "Archivo de Imagen")
Una vez creado todo lo anterior, realizamos un envio de mensaje y este iria con un forma ya fijo, cautivando la atencion desde un principio
![coamdo tree](32.jpg "Archivo de Imagen")
Posteriormente creamos una modificacion para mostar el topic, esto en el mismo archivo grafico
![coamdo tree](33.jpg "Archivo de Imagen")
Luego el archivo de `Contact.me`, creamos una variaple publica llamada `topic` y luego cambiamos el constuctor de la siguiente forma
![coamdo tree](34.jpg "Archivo de Imagen")
Creado esto volvemos a `ContactController` y volvemos a realizar un pequeño cambio
![coamdo tree](35.jpg "Archivo de Imagen")
Este cambio claramente se lograr evidenciar el inbox creado
![coamdo tree](36.jpg "Archivo de Imagen")
Para mejorar esto aun vamos a cambiar unas minisculas lineas
![coamdo tree](37.jpg "Archivo de Imagen")
Y gracias a esto tenimos ahora un subtitulo desente
![coamdo tree](38.jpg "Archivo de Imagen")

Finalizamos el capitulo

## 45) Send Email Using Markdown Templates

Para este capitulo se buscara realizar un envio de mensaje de manera mas grata por medio de Markdown

Para esto nos dirigimos al archivo de `ContactMe.php` que se encuentra en `Email`.
Para lo cual realizamos un pequeño cambio donde cambios el view por el markdown
![coamdo tree](39.jpg "Archivo de Imagen")

Realizado lo anterior nos vamos ahora al archivo de `contact-me.php`, eliminamos todo el contenido del archivo para crear el nuevo contenido con lo siguiente
![coamdo tree](40.jpg "Archivo de Imagen")
Si lo probamos en nos mostara el mensaje perfectamente pero tenemos que mirar mas haya y seguir programando para lo cual agregaremos un boton para que se mueste en el mensaje
![coamdo tree](41.jpg "Archivo de Imagen")

Queria de la siguiente forma con los cambios realizados
![coamdo tree](42.jpg "Archivo de Imagen")

Ahora bien probado lo siguiente vamos cambiar el titulo del texto, para ello nos entramos en el archivo de `.env` y cambiamos el apartado de `APP_NAME` por lo que queramos, por defecto siempre esta en Laracast
![coamdo tree](43.jpg "Archivo de Imagen")

Cambiado lo siguiente queria asi
![coamdo tree](44.jpg "Archivo de Imagen")

Ahora bien, seguimos con mas codicacion, en la cual tenemos que ingresar el siguiente comando que nos crear un archivo
![coamdo tree](45.jpg "Archivo de Imagen")

Realizado lo anterior, vamos y cambianos el envio del mensaje en cual ahora los realizara en `Contact()`
![coamdo tree](46.jpg "Archivo de Imagen")

Incoformes con esto crearemos cambios mas esteticos pero muy notorios, para ello primero colocaremos el siguiente comando
![coamdo tree](47.jpg "Archivo de Imagen")
Luego cambiamos el boton de color en el archivo de `button.blade.php`
![coamdo tree](48.jpg "Archivo de Imagen")

Todo quedaria de la siguiente forma
![coamdo tree](49.jpg "Archivo de Imagen")

Con este cambios pequeños queriamos un envio de mensaje mejorado por medio de markdown

## 46) Notifications Versus Mailables

Para este episodio se busca crea un metodo totalmente diferente a lo aterior, el cual busca realizar notificaciones del mensaje.

Para esto tendremos que crear aun conexion a `PaymentControler`
![coamdo tree](50.jpg "Archivo de Imagen")

Luego crearemos un usuario
![coamdo tree](51.jpg "Archivo de Imagen")

Entramos a la direccion le daremos al boton
![coamdo tree](52.jpg "Archivo de Imagen")

Esto nos dara un error dado que el boton nos envia a una direccion que no exite, para ello crearemos esta direccion y la funcion a la que quiero que ejecute
![coamdo tree](53.jpg "Archivo de Imagen")

Ahora bien vamos a PaymentController creamos la funcion de `store` con todos los datos necesarios
![coamdo tree](54.jpg "Archivo de Imagen")

Ya con esto funcionaria solo le cambiamos el mensaje que quierea que de la notificacion en `PaymentReceived`
![coamdo tree](55.jpg "Archivo de Imagen")
![coamdo tree](56.jpg "Archivo de Imagen")

## Laravel From The Scratch - 11 Notifications 47) Database Notifications

En este episodio se buscar un envio de mensaje desde cualquier numero canal y notificar al usuario 

Para esto, crearemos una tabla de noticacion con el siguiente linea de codigo
![coamdo tree](57.jpg "Archivo de Imagen")

Creada esta tabla nos vamos ahora a `PaymenReceived.php` y colocamos `database` en el retorno
![coamdo tree](58.jpg "Archivo de Imagen")

Realizamos una prueba para comprobar si los cambios fueron exitosos y se creo el dato en la base de datos
![coamdo tree](59.jpg "Archivo de Imagen")

Verificado esto, seguimos realizando cambien es este tocara ir la funcion sore y  realizar unos pequeños cambios
![coamdo tree](60.jpg "Archivo de Imagen")

De igual forma devemos ir a `PaymentReceived` y cambiar el constructor para ajustarlo a nuestras necesidades
![coamdo tree](61.jpg "Archivo de Imagen")

De la misma forma abra que hacer unos cambios en el array para a la hora de enviar la notificacion cargue los datos a la  base de datos
![coamdo tree](62.jpg "Archivo de Imagen")

Con se logra apreciar en la imagen
![coamdo tree](63.jpg "Archivo de Imagen")

Ahora bien compredido esto crearemos otra ruta, para mostrar las notificaciones
![coamdo tree](64.jpg "Archivo de Imagen")

Para ello tenemos que crear una clase que controle las notificaciones, para ello usamos el siguiente comando 
![coamdo tree](65.jpg "Archivo de Imagen")

Dentro  de esta clare creamos el siguiente codigo
![coamdo tree](66.jpg "Archivo de Imagen")

Asi mismo nos vamos a `notificacions` y `show`, este sera el archivo que mostara la clase show y cargara los datos de la base de datos
![coamdo tree](67.jpg "Archivo de Imagen")

Si probamos la ruta mostrara lo siguiente
![coamdo tree](68.jpg "Archivo de Imagen")

Ahora bien, realiaremos unos pequeños cambisos para cargar las notificaciones
![coamdo tree](69.jpg "Archivo de Imagen")

Asi mismo en el archivo de show cambiamos lineas para mostara los datos de notificaciones
![coamdo tree](70.jpg "Archivo de Imagen")

Realizamos un nuevo envio de mensajes
![coamdo tree](71.jpg "Archivo de Imagen")

Ahora vamos a la ruta de `notifications` y nos mostara ls los datos de la base de datos
![coamdo tree](72.jpg "Archivo de Imagen")

Esto seria los que nos muestra 
![coamdo tree](73.jpg "Archivo de Imagen")

Si quisieramos realizar mas cambios a la notificaciones, para ello volvemos a la funcion de `show`
![coamdo tree](74.jpg "Archivo de Imagen")

Realiamos un control de errores en caso de no haber notificaciones
![coamdo tree](75.jpg "Archivo de Imagen")

Para esto podemos reducir el codigo nuevamente del metodo de show.
![coamdo tree](76.jpg "Archivo de Imagen")

## 48)Send SMS Notifications in 5 Minutes

Para este episodio se busca enviar mensajes SMS a los usuarios
Primero nos dirigimos a la magina de `nexmo`
![coamdo tree](77.jpg "Archivo de Imagen")

Creado la seccion nos daran 2 euros para realizar pruebas 
![coamdo tree](78.jpg "Archivo de Imagen")

Luego realizamos una ejecucion de comandos
![coamdo tree](79.jpg "Archivo de Imagen")

Y nos vamos a la parte de configuracion de `nexmo`
![coamdo tree](80.jpg "Archivo de Imagen")

Luego en el archivo de `.env` creamo la siguiente lineas con los datos de configuracion
![coamdo tree](81.jpg "Archivo de Imagen")

Agregamos un numero para realizar el testeo de mensaje
![coamdo tree](82.jpg "Archivo de Imagen")

Luego nos vamos a `Numbers` para ver el numero que nos fue otorgado en la paguina
![coamdo tree](84.jpg "Archivo de Imagen")

Este numero sera utlizado para cologarlo con el siguiente codigo en el archivo de `config/services.php`
```
'nexmo' => [
    'sms_from' => '16193440522',
],

```

Asi se veria 
![coamdo tree](85.jpg "Archivo de Imagen")

Ahora bien nos vamos a`PaymendReceived.php` para colocar la linea de `nexmo` para asi ser utilizada
![coamdo tree](86.jpg "Archivo de Imagen")

En ese mismo archivo de debajo de la funcion ``toMail`, creamos lo siguiente
![coamdo tree](87.jpg "Archivo de Imagen")

Ahora bien en `User.php` creamos al final lo siguiente, si quieramos podemos pasar un texto vacio.
![coamdo tree](88.jpg "Archivo de Imagen")

Ya con esto podremos realizar una prueba dandole click al boton de enviar, este realizara la notificacion al numero registradoy con esto nos mostara lo siguiente
![coamdo tree](89.jpg "Archivo de Imagen")

Con esto terminamos este capitulo

## Laravel From The Scratch - 13 Authorizations 50) Limit Access to Authorized Users

En este episodio se buscara realizar algunas limitacione a los usuarios.

Primero creamos la ruta de los archivos de conersacion
![coamdo tree](90.jpg "Archivo de Imagen")

En el archivode `ConversationsController tendremos lo siguiente`
![coamdo tree](91.jpg "Archivo de Imagen")

En el archivo `index` entremos lo siguiente
![coamdo tree](92.jpg "Archivo de Imagen")

Asi mismo en el apartado de `show.blade.php` tendremos el siguiente codigo
![coamdo tree](93.jpg "Archivo de Imagen")

De la misma forma, en el archivod de `replies.blade.php` vamos a tener que escribirlo siguiente
![coamdo tree](94.jpg "Archivo de Imagen")

Ya con esto tenemos todo lo necesario para trabajar, ahora bien nos dirigimos al archivo que creamos la tabla de la conversacion y creamos un nuevo dato el cual sera 
![coamdo tree](95.jpg "Archivo de Imagen")

Si revisamos en la base de datos este aparecera `null`
![coamdo tree](96.jpg "Archivo de Imagen")

Volviendo al archivo de `replies.blade.php`, creamos un boton de para votar la mejor respiesta 
![coamdo tree](97.jpg "Archivo de Imagen")

Este aparecera inicialmente en todo las conversaciones
![coamdo tree](98.jpg "Archivo de Imagen")

Por lo cual debemos de controlar esto, por medio de la siguientes lineas
![coamdo tree](99.jpg "Archivo de Imagen")

Para esot escribimos el siguiente codigo
![coamdo tree](100.jpg "Archivo de Imagen")

Con esto controlarmos este error
![coamdo tree](101.jpg "Archivo de Imagen")

Pero bueno la idea de es controlar autorisaciones de usuario, para nos vamos `AuthServiceProcider` y ingresamos las siguientes lineas
![coamdo tree](102.jpg "Archivo de Imagen")

Esto nos permitira activar o no el boton de `Best Reply`, en este caso si lo dejamos en true esto se mantendra
![coamdo tree](103.jpg "Archivo de Imagen")

Muestra que todo se mantine normal
![coamdo tree](104.jpg "Archivo de Imagen")

Pero bueno lo idoneo seria cntorler los usuario a la hora de realizar el boto, para ello volvemos a cambiar la lineas de codificas
![coamdo tree](105.jpg "Archivo de Imagen")

ASi mismo nos vamos al archivo de `replies.blade.php` y creamos la accion que nos enviar el formulario al darle click al boton de `Best Reply`
![coamdo tree](106.jpg "Archivo de Imagen")

Sabiendo donde sera direccionado al darle click al boton debemos de crear esa ruta en `web.php`, asi mismo tenimos que crear el archivo de controller
![coamdo tree](107.jpg "Archivo de Imagen")

Para esot utilizamos el siguiente comando
![coamdo tree](108.jpg "Archivo de Imagen")

Dentro del archivo vamos a ingresar el siguiente codigo
![coamdo tree](109.jpg "Archivo de Imagen")

Ahora cada que realizamos un click al comentario, este crear el cambio en la base de dato,cuando el contero
![coamdo tree](110.jpg "Archivo de Imagen")

Muestra del cambio en la base de datos
![coamdo tree](111.jpg "Archivo de Imagen")

Pero biuento este necesita una autorizacion mejor para lo cual volvemos a controler de la conversacion y creamos la siguiente linea
![coamdo tree](112.jpg "Archivo de Imagen")

Recordar si si quitamos el `can` de la parte visual de `replies`
![coamdo tree](113.jpg "Archivo de Imagen")

No dara error al darl click al `Best Reply`, por lo que no cambiamos esto
![coamdo tree](114.jpg "Archivo de Imagen")

Pero bueno se trata se organizar mejor el codigo para ello primero creamo un modelo con el siguiente comando
![coamdo tree](115.jpg "Archivo de Imagen")

Posteriomente dentro del archivo eliminamos casi todo, dejando de la siguiente manera con el codigo ingresado
![coamdo tree](116.jpg "Archivo de Imagen")

ASi mismo vamos a `AuthServiceProvider` y quitamos el codigo que creamos antes
![coamdo tree](117.jpg "Archivo de Imagen")

Cambiamos la direccion de la funcion `store` por `update`
![coamdo tree](118.jpg "Archivo de Imagen")

En el archivo de `Conversation.php` creamo el siguiente codigo, para reubicar codigo
![coamdo tree](119.jpg "Archivo de Imagen")

Dejado de la siguiente forma
![coamdo tree](120.jpg "Archivo de Imagen")

Ahora bien recordar que tambien devemos de cambiar la ruta, en el archivo de `replies`
![coamdo tree](121.jpg "Archivo de Imagen")

Ahora bueno podemos mostara el ultimo voto que realizamos de una manera mas grafica de la siguiente forma
![coamdo tree](122.jpg "Archivo de Imagen")

Graficamente se veria asi
![coamdo tree](123.jpg "Archivo de Imagen")

Pero bueno esto se puede reorganizar mejor, por lo cual vamos al archivo de `Reply.php` y creamos la siguiente funcion
![coamdo tree](124.jpg "Archivo de Imagen")

Luego cambiamos la lineas anterimente creada por la siguiente y con esto mejoramos el codigo de una forma mas organizada y estizada
![coamdo tree](125.jpg "Archivo de Imagen")

Con ello finalizamos este capitulo, que limita los usuario

## 51) Authorization Filters

En esta lección, revisaremos la estructura básica, cómo rastrear la clase subyacente y cuándo puede optar por no usarlas.

Primero nos vamos a `PagesController`, en ella creamos un direccion a `welcome`, comprobamos que todo valla bien
![coamdo tree](126.jpg "Archivo de Imagen")

Una vez comprobaso ahora escribimos lo siguiente para cargar el nombre de la url
![coamdo tree](127.jpg "Archivo de Imagen")

Como se logra apreciar en la direccion el nomber es `john` y ese saria el que mostraria
![coamdo tree](128.jpg "Archivo de Imagen")

Asi mismo no muestra otra forma de realizar lo mismo con `Request`
![coamdo tree](129.jpg "Archivo de Imagen")

Tambien nombre muestra una serie de comandos como seria el `key`
![coamdo tree](130.jpg "Archivo de Imagen")

El comadno e `request`
![coamdo tree](131.jpg "Archivo de Imagen")

La carga de un archivo por medio el comando `File`
![coamdo tree](132.jpg "Archivo de Imagen")

Asi se veria en el archivo 
![coamdo tree](133.jpg "Archivo de Imagen")

Y en la parte grafica quedaria asi
![coamdo tree](134.jpg "Archivo de Imagen")

Tambien lo podiriamos realizar de la siguiente forma 
![coamdo tree](135.jpg "Archivo de Imagen")

Siguiento con la explicacion podremos cuadar datos en la cache
![coamdo tree](136.jpg "Archivo de Imagen")

Y para no olvidar el view este,  en el mismo que el primero, ya que llevan al mismo punto solo cambia la sintacsis
![coamdo tree](137.jpg "Archivo de Imagen")

Con esto terminamos el capitulo

## 52) Guessing the Ability Name

Buscaramo como adivinar el nombre de la abilidad por medio de `Laravel`

Para esto hay que dirigimos al archivo de `AuthozesRequest.php`, este se encuentra en `Auth` y en `Access`
![coamdo tree](138.jpg "Archivo de Imagen")

Posteriomente en `replies` cambiamos el `update` por el `create`
![coamdo tree](139.jpg "Archivo de Imagen")

De igual forma cambiamos el nombre de la funcion en `ConversationBestReplyController`, tambien por `create`
![coamdo tree](140.jpg "Archivo de Imagen")

 Por ultimo realizamos los mismo en el archivo de `replies`y ya con esto funcionara. Pero esto seria una forma se hacer que Laravel adivine, pero es recomendado mejor idicar la direccion para que otras personas que valla a ver el codigo lo comprendan rapidamente
![coamdo tree](141.jpg "Archivo de Imagen")

Fin del capitulo

## 53) Middleware-Based Authorization

En este episodio se busca la forma como manejar las autorzaciones desde un middleware específico de ruta. 

Para esto podemos inicialmente ir al archivo de `ConversationsController` y crear la siguiente linea
![coamdo tree](142.jpg "Archivo de Imagen")

Con esto si recargamos la paquina de notificaciones esta me enviara un error
![coamdo tree](143.jpg "Archivo de Imagen")

Para solucionar este detalle podeos irnos a `ConversationPOlicy` y crear la siguiente funcion, con esto ya nos cargara todo perfectamente
![coamdo tree](144.jpg "Archivo de Imagen")

Pero bueno volviendo al archivo de `ConversationsController`, volvemos a cambiarlo para que quede asi la la funcion de `show`
![coamdo tree](145.jpg "Archivo de Imagen")

Cambiamos la ruta luego y con esto tenemos un controler de autirizacion
![coamdo tree](146.jpg "Archivo de Imagen")

Dado que si actualizamos nos enviara este mensaje
![coamdo tree](147.jpg "Archivo de Imagen")

Con esto finalizamos este capitulo

## 54) Roles and Abilities

Para este capitulo se busca construir un sistema de autorización completo basado en roles que nos permita otorgar y revocar dinámicamente varias habilidades por usuario

Para esto primero crearemos la tabla de roles con el siguiente comando
![coamdo tree](148.jpg "Archivo de Imagen")

Luego nos vamos donde esta el archivo y en primera instancia elinamos todo lo que tenga la funcion de `down` y de `up`
![coamdo tree](149.jpg "Archivo de Imagen")

Ahora bien dentro de la fucion `up`, vamos a crear varias estructuras de tablas las cuales seria `roles`, `abilities`, `ability_role` y `role_user`. Nota importante para cada esquema hay que cambiar el table por `create`, sino nos dara error
![coamdo tree](150.jpg "Archivo de Imagen")
![coamdo tree](151.jpg "Archivo de Imagen")

Este table si no lo noto
![coamdo tree](152.jpg "Archivo de Imagen")

Con este comando creara las tablas

![coamdo tree](153.jpg "Archivo de Imagen")

Si nos vamos a la base de datos hay aparece todas las tablas creadas
![coamdo tree](154.jpg "Archivo de Imagen")

Ahora bien vamos a crear dos modelos el de `Role` y `Ability`, con los siguientes comandos
![coamdo tree](155.jpg "Archivo de Imagen")
![coamdo tree](156.jpg "Archivo de Imagen")

Ahora nos dirigimos al archivo de role y creamos el siguiente codigo
![coamdo tree](157.jpg "Archivo de Imagen")

Ahora cambiamos nuevamente al archivo de `Ability` y creamos las siguientes lineas
![coamdo tree](158.jpg "Archivo de Imagen")

Seguidamente abrimos a `User.php` y creamos la funcion de `roles`, y de `assignRole`
![coamdo tree](159.jpg "Archivo de Imagen")

Ahora volvemos a `Role.php` y agregamos las siguientes lineas para que crear el timestamps
![coamdo tree](160.jpg "Archivo de Imagen")

De la misma forma se los aplicanos a `Ability`
![coamdo tree](161.jpg "Archivo de Imagen")

Y por ultimo para no perder el ritmo camos a plicarselo a la funcion de roles en `User.php`
![coamdo tree](162.jpg "Archivo de Imagen")

Ahora ejecutamos estos comandos
![coamdo tree](163.jpg "Archivo de Imagen")
![coamdo tree](164.jpg "Archivo de Imagen")

Seguidamente abrimos `Tinkerwell` para ejecutar las siguientes lineas

![coamdo tree](165.jpg "Archivo de Imagen")

Resultado
![coamdo tree](166.jpg "Archivo de Imagen")

Cuando ejecutamos ese comando nos dara error
![coamdo tree](167.jpg "Archivo de Imagen")

Esto se debe a que en el apartadode `User.php`, le hace falta la siguiente linea
![coamdo tree](168.jpg "Archivo de Imagen")

Ahora si se ejecuta bien
![coamdo tree](169.jpg "Archivo de Imagen")

Pero como si los problemas no acaban, nos vuelve a dar error con el siguiente comando
![coamdo tree](170.jpg "Archivo de Imagen")

Para ello tenemos que colocar el `protected`, tanto en Role como en Ability
![coamdo tree](171.jpg "Archivo de Imagen")
![coamdo tree](172.jpg "Archivo de Imagen")

Proseguimos ejecutado comandos, procure escribilo exacto.
![coamdo tree](173.jpg "Archivo de Imagen")
![coamdo tree](174.jpg "Archivo de Imagen")
![coamdo tree](175.jpg "Archivo de Imagen")
![coamdo tree](176.jpg "Archivo de Imagen")
![coamdo tree](177.jpg "Archivo de Imagen")
![coamdo tree](178.jpg "Archivo de Imagen")
![coamdo tree](179.jpg "Archivo de Imagen")
![coamdo tree](180.jpg "Archivo de Imagen")
![coamdo tree](181.jpg "Archivo de Imagen")
![coamdo tree](182.jpg "Archivo de Imagen")
![coamdo tree](183.jpg "Archivo de Imagen")


Luego de aver ejecuta varios comando, hay que dirigirnos un instantes a `User.php` y crear la siguiente linea

![coamdo tree](184.jpg "Archivo de Imagen")

Creado esto seguimos ejecutando mas lineas
![coamdo tree](185.jpg "Archivo de Imagen")

Otras vez toca cambiar de archivo, hay que ir a `welcome.blade.php`, eliminamos todo lo que esta en medio `body` y digitaremos lo siguiente como esta en la foto
![coamdo tree](186.jpg "Archivo de Imagen")

Luego vamos a `AuthServiceProvider.php` y creamos la siguiente linea, se que repetivo, pero es lo necesario para que todo esto funciona de la mejor manera
![coamdo tree](187.jpg "Archivo de Imagen")

Luego en `Web.php` el que se encuentra en rutas, creamos la siguiente linea con el id de mi usuario

![coamdo tree](188.jpg "Archivo de Imagen")

Con esto ya podemos recargar la pagina y nos mostarar el enlace que habiamos creado con los comandos anteriores en `Tinkerwell`
![coamdo tree](189.jpg "Archivo de Imagen")

Si eliminamos la relacion, solo se cargaria el titulo

![coamdo tree](190.jpg "Archivo de Imagen")

Y nos daria un error
![coamdo tree](191.jpg "Archivo de Imagen")

Para ello tenemos que solucionarlo dirigiendonos a `User.php`, dejado la funcion de assignRole de la siguiente forma
![coamdo tree](192.jpg "Archivo de Imagen")

Seguimos realizando varios comando crear datos y relacione
![coamdo tree](193.jpg "Archivo de Imagen")
![coamdo tree](194.jpg "Archivo de Imagen")

Ahora que creamos una relacion de `view_report`, refrecamos la pagina y nos da otro fallo

![coamdo tree](195.jpg "Archivo de Imagen")

No cargo bien
![coamdo tree](196.jpg "Archivo de Imagen")

Si vemos nuevamente al TinkerWell vemos el erro y nos da una pista donde va el error

![coamdo tree](197.jpg "Archivo de Imagen")

Para ello de la misma forma que la vez pasada, apara que cambiar los datos en este caso del archivo `Role`

![coamdo tree](198.jpg "Archivo de Imagen")

Reparado esto seguimos ejecutando comando
![coamdo tree](199.jpg "Archivo de Imagen")

Luego nos vamos la direccion de `view_report`, y le asignamos una
![coamdo tree](200.jpg "Archivo de Imagen")

Luego en `Web.php` y redireccionamos esa ruta

![coamdo tree](201.jpg "Archivo de Imagen")

Al carga la direccion no dara el mensaje
![coamdo tree](202.jpg "Archivo de Imagen")

Ahora bien, continuamente luego de los escrito en `Web.php`, colocamos lo siguiente
![coamdo tree](203.jpg "Archivo de Imagen")

Esto nos inpedira acceder de invendiato por cuestion de acceso
![coamdo tree](204.jpg "Archivo de Imagen")

Luego otras vez, otro falla
![coamdo tree](205.jpg "Archivo de Imagen")

Para solucinar esto debemos ir a la Funcion de `assignRole` en `User.php`

![coamdo tree](206.jpg "Archivo de Imagen")

Ya con esto solucionamos el fallo

![coamdo tree](207.jpg "Archivo de Imagen")

Podemos recargar la pagina perfectamente
![coamdo tree](208.jpg "Archivo de Imagen")

Para finalizar ceamos esta linea en `Role.php` para filtar fallo que se puedan dar.
![coamdo tree](209.jpg "Archivo de Imagen")

Fin del capitulo